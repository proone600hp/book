package com.example.tseysembayev.book;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class BookAdapter extends ArrayAdapter<Book> {

    //private static final String LOCATION_SEPARATOR = " of ";

    public BookAdapter(Context context, List<Book> books) {
        super(context, 0, books);
    }

    /**
     * Returns a list item view that displays information about the earthquake at the given position
     * in the list of earthquakes.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if there is an existing list item view (called convertView) that we can reuse,
        // otherwise, if convertView is null, then inflate a new list item layout.
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.book_list, parent, false);
        }

        // Find the earthquake at the given position in the list of earthquakes
        Book currentBook = getItem(position);

        TextView titleView = (TextView) listItemView.findViewById(R.id.book_title_text_view);
        titleView.setText(currentBook.getmTitle());

        TextView publisherView = (TextView) listItemView.findViewById(R.id.publisher_text_view);
        publisherView.setText(currentBook.getmPublisher());

        TextView authorView = (TextView) listItemView.findViewById(R.id.author_name);
        // Display the location of the current earthquake in that TextView
        authorView.setText(currentBook.getmAuthor());

        // Find the TextView with view ID location offset
        ImageView imageView = (ImageView) listItemView.findViewById(R.id.book_thumbnail);
        // Display the location offset of the current earthquake in that TextView
        Glide.with(getContext()).load(currentBook.getmImage()).into(imageView);
        //imageView.setImageResource(currentBook.getmImage());
        TextView dateView = (TextView) listItemView.findViewById(R.id.publish_date);
        // Display the location of the current earthquake in that TextView
        dateView.setText(currentBook.getmPublishDate());
        // Create a new Date object from the time in milliseconds of the earthquake
//        Date dateObject = new Date(currentBook.getmPublishDate());
//
//        // Find the TextView with view ID date
//        TextView dateView = (TextView) listItemView.findViewById(R.id.publish_date);
//        // Format the date string (i.e. "Mar 3, 1984")
//        String formattedDate = formatDate(dateObject);
//        // Display the date of the current earthquake in that TextView
//        dateView.setText(formattedDate);

        // Return the list item view that is now showing the appropriate data
        return listItemView;
    }


    /**
     * Return the formatted date string (i.e. "Mar 3, 1984") from a Date object.
     */
    private String formatDate(Date dateObject) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("LLL dd, yyyy");
        return dateFormat.format(dateObject);
    }
}
