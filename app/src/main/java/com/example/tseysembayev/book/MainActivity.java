package com.example.tseysembayev.book;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity  implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String LOG_TAG = MainActivity.class.getName();

    /**
     * Constant value for the earthquake loader ID. We can choose any integer.
     * This really only comes into play if you're using multiple loaders.
     */
    private static final int BOOK_LOADER_ID = 1;

    /**
     * Adapter for the list of earthquakes
     */
    private BookAdapter mAdapter;

    /**
     * TextView that is displayed when the list is empty
     */
    private TextView mEmptyStateTextView;
    String USGS_REQUEST_URL = "https://www.googleapis.com/books/v1/volumes?";
    private Spinner mSpinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Find a reference to the {@link ListView} in the layout
        ListView bookListView = (ListView) findViewById(R.id.list);
        mSpinner = (Spinner) findViewById(R.id.spinner);
        mEmptyStateTextView = (TextView) findViewById(R.id.empty_view);
        bookListView.setEmptyView(mEmptyStateTextView);

        // Create a new adapter that takes an empty list of earthquakes as input
        mAdapter = new BookAdapter(this, new ArrayList<Book>());

        // Set the adapter on the {@link ListView}
        // so the list can be populated in the user interface
        bookListView.setAdapter(mAdapter);
        // Obtain a reference to the SharedPreferences file for this app
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        // And register to be notified of preference changes
        // So we know when the user has adjusted the query settings
        prefs.registerOnSharedPreferenceChangeListener(this);
        // Set an item click listener on the ListView, which sends an intent to a web browser
        // to open a website with more information about the selected earthquake.
        bookListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                // Find the current earthquake that was clicked on
                Book currentBook = mAdapter.getItem(position);

                // Convert the String URL into a URI object (to pass into the Intent constructor)
                Uri bookUri = Uri.parse(currentBook.getmUrl());

                // Create a new intent to view the earthquake URI
                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, bookUri);

                // Send the intent to launch a new activity
                startActivity(websiteIntent);
            }
        });
        // Get a reference to the ConnectivityManager to check state of network connectivity
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);

        // Get details on the currently active default data network
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        // If there is a network connection, fetch data
        if (networkInfo != null && networkInfo.isConnected()) {
            // Get a reference to the LoaderManager, in order to interact with loaders.
            LoaderManager loaderManager = getLoaderManager();

            // Initialize the loader. Pass in the int ID constant defined above and pass in null for
            // the bundle. Pass in this activity for the LoaderCallbacks parameter (which is valid
            // because this activity implements the LoaderCallbacks interface).
            loaderManager.initLoader(BOOK_LOADER_ID, null, mLoaderCallbacks);
        } else {
            // Otherwise, display error
            // First, hide loading indicator so error message will be visible
            View loadingIndicator = findViewById(R.id.loading_indicator);
            loadingIndicator.setVisibility(View.GONE);

            // Update empty state with no connection error message
            mEmptyStateTextView.setText(R.string.no_internet_connection);
        }

        restartButton();
    }


//    public String url() {
//        EditText nameField = (EditText) findViewById(R.id.search_edit_text);
//        Editable nameEditable = nameField.getText();
//        String name = nameEditable.toString();
//        String name2 = "android";
//        String USGS_REQUEST_URL = "https://www.googleapis.com/books/v1/volumes?q";
//        return USGS_REQUEST_URL;
//    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.settings_min_magnitude_key)) ||
                key.equals(getString(R.string.settings_order_by_key))){
            // Clear the ListView as a new query will be kicked off
            mAdapter.clear();

            // Hide the empty state text view as the loading indicator will be displayed
            mEmptyStateTextView.setVisibility(View.GONE);

            // Show the loading indicator while new data is being fetched
            View loadingIndicator = findViewById(R.id.loading_indicator);
            loadingIndicator.setVisibility(View.VISIBLE);

            // Restart the loader to requery the USGS as the query settings have been updated
            getLoaderManager().restartLoader(BOOK_LOADER_ID, null, mLoaderCallbacks);
        }
    }
    /**
     * Setup the dropdown spinner that allows the user to select the gender of the pet.
     */
    private void setupSpinner() {
        // Create adapter for spinner. The list options are from the String array it will use
        // the spinner will use the default layout
        ArrayAdapter <String>  adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);

        // Specify dropdown layout style - simple list view with 1 item per line
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        // Apply the adapter to the spinner
        mSpinner.setAdapter(adapter);

        // Set the integer mSelected to the constant values
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedText = mSpinner.getSelectedItem().toString();
                SharedPreferences.Editor editor = getSharedPreferences("Print Type", MODE_PRIVATE).edit();
                editor.putString("books", selectedText);
                editor.putString("magazines", selectedText);
                editor.commit();
            }

            // Because AdapterView is an abstract class, onNothingSelected must be defined
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                String def = "all";
            }
        });
    }
    // Our Callbacks. Could also have the Activity/Fragment implement
    // LoaderManager.LoaderCallbacks<List<String>>
    private LoaderManager.LoaderCallbacks<List<Book>>
            mLoaderCallbacks =
            new LoaderManager.LoaderCallbacks<List<Book>>() {
                @Override
                public Loader<List<Book>> onCreateLoader(int i, Bundle bundle) {

                    SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                    String maxResults = sharedPrefs.getString(
                            getString(R.string.settings_min_magnitude_key),
                            getString(R.string.settings_min_magnitude_default));

                    String printType = sharedPrefs.getString(
                            getString(R.string.settings_order_by_key),
                            getString(R.string.settings_order_by_default)
                    );

                    EditText nameField = (EditText) findViewById(R.id.search_edit_text);
                    Editable nameEditable = nameField.getText();
                    String name = nameEditable.toString();
                    // Create a new loader for the given URL
                    //https://www.googleapis.com/books/v1/volumes?q=java+core&maxResults=10
                    Uri baseUri = Uri.parse(USGS_REQUEST_URL);
                    Uri.Builder uriBuilder = baseUri.buildUpon();

                    uriBuilder.appendQueryParameter("q", name);
                    uriBuilder.appendQueryParameter("maxResults", maxResults);
                    uriBuilder.appendQueryParameter("printType", printType);
                    Log.v(LOG_TAG, uriBuilder.toString());
                    return new BookLoader(getApplicationContext(), uriBuilder.toString());
                }

                @Override
                public void onLoadFinished(Loader<List<Book>> loader, List<Book> books) {
                    // Hide loading indicator because the data has been loaded
                    View loadingIndicator = findViewById(R.id.loading_indicator);
                    loadingIndicator.setVisibility(View.GONE);

                    // Set empty state text to display "No earthquakes found."
                    mEmptyStateTextView.setText(R.string.no_books);

                    // Clear the adapter of previous earthquake data
                    //mAdapter.clear();

                    // If there is a valid list of {@link Earthquake}s, then add them to the adapter's
                    // data set. This will trigger the ListView to update.
                    if (books != null && !books.isEmpty()) {
                        mAdapter.addAll(books);
                        //updateUi(earthquakes);
                    }
                }

                @Override
                public void onLoaderReset(Loader<List<Book>> loader) {
                    // Loader reset, so we can clear out our existing data.
                    mAdapter.clear();
                }

            };

    public void restartButton() {
        ImageView search = (ImageView) findViewById(R.id.search_ico);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.clear();
                getLoaderManager().restartLoader(BOOK_LOADER_ID, null, mLoaderCallbacks).forceLoad();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

