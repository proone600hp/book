package com.example.tseysembayev.book;

public class Book {

    private String mImage, mTitle, mAuthor, mPublishDate, mPublisher, mUrl;
    //private int mImage;

    public Book(String image, String title, String author, String publishDate, String publisher, String url) {
        mImage = image;
        mTitle = title;
        mAuthor = author;
        mPublishDate = publishDate;
        mPublisher = publisher;
        mUrl = url;
    }

    public Book(String image, String title, String publishDate, String publisher, String infoLink) {
        mImage = image;
        mTitle = title;
        mPublishDate = publishDate;
        mPublisher = publisher;
        mUrl = infoLink;
    }

    public String getmImage() {
        return mImage;
    }
    public String getmTitle() {
        return mTitle;
    }
    public String getmAuthor() {
        return mAuthor;
    }
    public String getmPublishDate() {
        return mPublishDate;
    }
    public String getmPublisher() {
        return mPublisher;
    }
    public String getmUrl() {
        return mUrl;
    }
}
